![logo]

* tiny and light JavaScript project
* [8-bit colors] only
* no dependencies to slow down its performance
* less than 100 lines of code

## Put it on your website!

Put this sleek widget on your website to change colors easily and with
style.

* Add the `eightbitcolorpicker.js` file to your website either by adding
it as a script tag in the HTML file or by adding it to the source code
directory that you have set up for your prefered web packing tool.
* Configure it by changing the tiny, clean and readable code. 
* Add `EightBitCP` to every element that you want to change the color of.

## It's fun to Use!

* Click on the element to open the color-picker window.
* Click on a color in the color-picker to instantly change it's color.
* Double click on a color for the color-picker to close.
* Or click again on the element that opened the color-picker to close
it.
* [Test it here!]

[logo]: logo.png

[8-bit colors]: https://en.wikipedia.org/wiki/8-bit_color
[Test it here!]: https://damavrom.gitlab.io/eight-bit-color-picker/demo.html
