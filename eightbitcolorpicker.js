CELL_SIZE=20;

window.onscroll = closeColorPicker;
window.onresize = closeColorPicker;
document.onclick = function(e) {
    const EightBitCP = document.getElementById("color-picker");
    if (EightBitCP.style.display == "none") {
        if (e.target.className.match(/EightBitCP/))
            openColorPicker(e.target);
    }
    else if (typeof(e.target.className) != "string")
        closeColorPicker();
    else if (!e.target.className.match(/cell/))
        closeColorPicker();
}

function installColorPicker() {
    console.log("inicializing color picker");
    const EightBitCP = document.createElement('div');
    EightBitCP.style.width = (16*CELL_SIZE)+"px";
    EightBitCP.style.display = "none";
    EightBitCP.style.zIndex = "10";
    EightBitCP.style.borderRadius = "5px";
    EightBitCP.style.overflow = "hidden";
    EightBitCP.style.boxShadow = "0px 0px 15px #444";
    EightBitCP.id = "color-picker";
        
    EightBitCP.filter = document.createElement('div');
    EightBitCP.filter.style.display = "none";
    EightBitCP.filter.style.position = "fixed";
    EightBitCP.filter.style.top = "0px";
    EightBitCP.filter.style.left = "0";
    EightBitCP.filter.style.width = "200%";
    EightBitCP.filter.style.height = "200%";
    EightBitCP.filter.style.transform = "translate(-50%, -50%)";
    EightBitCP.filter.style.pointerEvents = "none";
    //EightBitCP.filter.style.backgroundColor = "#a0a0a0a0";

    for (let i = 0; i < 256; i++) {
        const cell = document.createElement("div");
        cell.className = "cell";
        cell.style.display = "inline-block";
        cell.style.verticalAlign = "top";
        cell.style.width = CELL_SIZE+"px";
        cell.style.height = CELL_SIZE+"px";
        const red = Math.round(36.42*Math.floor((i%32)/4));
        const green = Math.round(36.42*Math.floor(i/32));
        const blue = 85*(i%4);
        const alpa = 0;
        cell.style.backgroundColor = "rgb("
            +red+", "
            +green+", "
            +blue+")";
        cell.lastClick = 0;
        cell.onclick = function() {
            this.parentElement.target.style.backgroundColor = this.style.backgroundColor;
            const now = new Date();
            if (now-this.lastClick < 300) {
                cell.lastClick = 0;
                closeColorPicker();
            } else {
                cell.lastClick = now;
            }
        }
        EightBitCP.appendChild(cell);
    }
    document.body.appendChild(EightBitCP);
    document.body.appendChild(EightBitCP.filter);
}

function openColorPicker(el) {
    const EightBitCP = document.getElementById("color-picker");
    EightBitCP.style.display = "block";
    EightBitCP.filter.style.display = "block";
    EightBitCP.target = el;
    const cpWidth = EightBitCP.getBoundingClientRect()['width'];
    const cpHeight = EightBitCP.getBoundingClientRect()['height'];
    const targetRect = el.getBoundingClientRect();
    const targetCenterY = (targetRect["left"]+targetRect["right"])/2;
    const targetCenterX = (targetRect["top"]+targetRect["bottom"])/2;
    EightBitCP.filter.style.backgroundPosition = targetCenterY+"px "+targetCenterX+"px";
    EightBitCP.filter.style. backgroundImage = 
        "radial-gradient("
        +"circle, "
        +"#00000000 "+(targetRect["height"]/2)+"px, "
        +"#00000050 "+(targetRect["height"]*2)+"px, "
        +"#000000c0 50%)";

    if (window.innerWidth/2 > cpWidth) {
        EightBitCP.style.position = "fixed";
        EightBitCP.style.top = "50%";
        EightBitCP.style.transform = "translate(-50%, -50%)";
        if (window.innerWidth/3 > cpWidth) {
            if (targetCenterY < window.innerWidth/2) {
                EightBitCP.style.left = "66%";
            } else {
                EightBitCP.style.left = "33%";
            }
        } else {
            if (targetCenterY < window.innerWidth/2) {
                EightBitCP.style.left = "75%";
            } else {
                EightBitCP.style.left = "25%";
            }
        }
    } else {
        if (window.innerHeight-10-targetRect["bottom"] > cpHeight) {
            EightBitCP.style.position = "fixed";
            EightBitCP.style.transform = "translate(-50%, 0%)";
            EightBitCP.style.left = "50%";
            EightBitCP.style.top = (5+targetRect["bottom"])+"px";
        } else if (targetRect["top"]-10 > cpHeight) {
            EightBitCP.style.position = "fixed";
            EightBitCP.style.transform = "translate(-50%, 0%)";
            EightBitCP.style.left = "50%";
            EightBitCP.style.top = (targetRect["top"]-cpHeight-5)+"px";
        } else {
            EightBitCP.style.display = "none";
            window.alert("not enough space for color picker to open");
        }
    }
}

function closeColorPicker() {
    const EightBitCP = document.getElementById("color-picker");
    EightBitCP.style.display = "none";
    EightBitCP.filter.style.display = "none";
    EightBitCP.target = null;
}

installColorPicker();
